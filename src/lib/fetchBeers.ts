import { BeersResults } from "@/models/Beer";

async function fetchBeers(url: string): Promise<BeersResults | undefined> {
  try {
    const res = await fetch(url);

    if (!res.ok) throw new Error("Fetch beers error\n");

    const beersResults: BeersResults = await res.json();

    return beersResults;
  } catch (e) {
    if (e instanceof Error) console.log(e.stack);
  }
}

export { fetchBeers };
