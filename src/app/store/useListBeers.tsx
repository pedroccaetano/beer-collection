import { create } from 'zustand'

import { Beer } from '@/models/Beer'

export type SortByType = '' | 'nameAZ' | 'nameZA' | 'brewingDateAsc' | 'brewingDateDesc'

interface ListBeersState {
  beers: Beer[]
  initialBeers: Beer[]
  addNewBeer: (beers: Beer) => void
  sortBy: SortByType
  setBeers: (beers: Beer[]) => void
  setSortBy: (type: SortByType) => void
  filterBy: { 'name': string | '', 'abv': number | '', 'ibu': number | '' }
  setFiltersBy: (name: string, value: string | number) => void
  possibleFilters: () => { 'abv': null | number[], 'ibu': null | number[] }
}

const addFormattedDate = (beer: Beer) => {
  const [month, year] = beer.first_brewed.split("/");
  return { ...beer, formatted_first_brewed: new Date(parseInt(year), parseInt(month) - 1) };
}

const getPossibleFilters = (beers: Beer[]): { 'abv': null | number[], 'ibu': null | number[] } => {
  if (beers.length === 0) return { 'abv': null, 'ibu': null }

  const abvSet = new Set();
  const ibuSet = new Set();

  for (const beer of beers) {
    abvSet.add(beer.abv);
    ibuSet.add(beer.ibu);
  }

  const sortedAbv = [...abvSet].sort() as number[];
  const sortedIbu = [...ibuSet].sort() as number[];

  return { 'abv': sortedAbv, 'ibu': sortedIbu };
}

const filterBeers = (beers: Beer[], filterBy: { 'name': string | '', 'abv': number | '', 'ibu': number | '' }) => beers
  .filter((beer) => {
    if (filterBy.name && !beer.name.toLowerCase().includes(filterBy.name.toLowerCase())) return false
    if (filterBy.abv && beer.abv !== filterBy.abv) return false
    if (filterBy.ibu && beer.ibu !== filterBy.ibu) return false
    return true
  })

const useListBeersStore = create<ListBeersState>()(
  (set, get) => ({
    beers: [],
    initialBeers: [],
    sortBy: '',
    filterBy: { 'name': '', 'abv': '', 'ibu': '' },
    addNewBeer: (beer: Beer) => {
      const arrayOfNewBeer = [...get().initialBeers, beer]

      set({ beers: arrayOfNewBeer, initialBeers: arrayOfNewBeer })

      get().setSortBy(get().sortBy)
    },
    setBeers: (beers: Beer[]) => set({ beers, initialBeers: beers }),
    setSortBy: (sortBy) => {
      let sortedBeers: Beer[] = [...get().initialBeers]

      switch (sortBy) {
        case 'nameAZ':
          sortedBeers = filterBeers(sortedBeers.sort((a, b) => a.name < b.name ? -1 : 1), get().filterBy)
          break
        case 'nameZA':
          sortedBeers = filterBeers(sortedBeers.sort((a, b) => a.name > b.name ? -1 : 1), get().filterBy)
          break
        case 'brewingDateAsc':
          sortedBeers = filterBeers(sortedBeers
            .map(addFormattedDate)
            .sort((a, b) => a.formatted_first_brewed < b.formatted_first_brewed ? -1 : 1), get().filterBy)
          break
        case 'brewingDateDesc':
          sortedBeers = filterBeers(sortedBeers
            .map(addFormattedDate)
            .sort((a, b) => a.formatted_first_brewed > b.formatted_first_brewed ? -1 : 1), get().filterBy)
          break
      }

      set({ sortBy, beers: sortedBeers })
    },
    setFiltersBy: (name: string, value: string | Number) => {
      const filterBy = { ...get().filterBy, [name]: value }

      set({ filterBy })

      if (get().sortBy !== '') {
        get().setSortBy(get().sortBy)
      } else {
        const filteredBeers = filterBeers(get().initialBeers, filterBy)
        set({ beers: filteredBeers })
      }
    },

    possibleFilters: () => getPossibleFilters(get().initialBeers || [])
  }),
)

export default useListBeersStore