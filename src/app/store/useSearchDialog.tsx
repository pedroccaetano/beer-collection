import { create } from 'zustand'

interface SearchState {
  state: boolean
  close: () => void
  open: () => void
}

const useSearchDialogStore = create<SearchState>()(
  (set) => ({
    state: false,
    close: () => set(({ state: false })),
    open: () => set(({ state: true })),
  }),
)

export default useSearchDialogStore