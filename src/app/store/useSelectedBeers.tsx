import { create } from 'zustand'
import { devtools, persist } from 'zustand/middleware'

import { fetchBeers } from '@/lib/fetchBeers'
import { Beer, SelectedBeer } from '@/models/Beer'
import useListBeersStore from '@/app/store/useListBeers'

interface SelectedBeersState {
  selectedBeers: SelectedBeer[]
  remove: (id: number) => void
  add: (beer: Beer) => void
  addRate: (id: number, rate: number) => void
  addComment: (id: number, comment: string) => void
  fetchBeers: () => void
}

const useSelectedBeersStore = create<SelectedBeersState>()(
  devtools(
    persist(
      (set, get) => ({
        selectedBeers: [],
        remove: (id) => set((state) => ({ selectedBeers: state.selectedBeers.filter((beer) => beer.id !== id) })),
        add: (beer) => {
          useListBeersStore.getState().addNewBeer(beer)

          set((state) => ({ selectedBeers: [...state.selectedBeers, { id: beer.id }] }))
        },
        addRate: (id, rate) => {


          set((state) => ({
            selectedBeers: state.selectedBeers.map((beer) => {
              if (beer.id === id) {
                return { ...beer, rate }
              }
              return beer
            })
          }))
        },
        addComment: (id, comment) => set((state) => ({
          selectedBeers: state.selectedBeers.map((beer) => {
            if (beer.id === id) {
              return { ...beer, comment }
            }
            return beer
          })
        })),
        fetchBeers: async () => {
          const formattedIds = get().selectedBeers.map((beer) => beer.id).join('|')

          const beersResults = await fetchBeers(`https://api.punkapi.com/v2/beers?ids=${formattedIds}`);

          if (beersResults) {
            useListBeersStore.getState().setBeers(beersResults)
          }
        }
      }),
      {
        name: 'beers@selectedBeers',
      }
    )
  )
)

export default useSelectedBeersStore