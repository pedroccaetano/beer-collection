import Sorting from './sorting'
import Filtering from './filtering'

export default function FilterAndSorting() {
  return (
    <section className='mb-4 flex flex-col lg:flex-row lg:justify-between gap-2'>
      <Filtering />
      <Sorting />
    </section>
  )
}
