import useListBeersStore from '@/app/store/useListBeers'
import Select from '../Select'

const selectClass = "flex w-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-[#ef9c0a] focus:border-[#ef9c0a] lg:w-[256px] p-2 outline outline-transparent"

export default function Filtering() {
  const filterBy = useListBeersStore(state => state.filterBy)
  const setFiltersBy = useListBeersStore(state => state.setFiltersBy)
  const possibleFilters = useListBeersStore(state => state.possibleFilters)()

  return (
    <div className='flex flex-col lg:flex-row items-center gap-2'>
      <input
        type="text"
        value={filterBy.name || ''}
        placeholder="Find your beer"
        aria-label="Search for a beer by name"
        onChange={(e) => setFiltersBy('name', e.target.value)}
        className={`${selectClass} lg:min-w-[256px]`}
      />

      <div className="flex gap-2 w-full">
        <Select
          name="alcoholByVolume"
          value={filterBy.abv || ''}
          disabled={!possibleFilters.abv}
          ariaLabel="Filter by Alcohol by Volume"
          onChange={(e) => setFiltersBy('abv', Number(e.target.value))}
          options={possibleFilters.abv || []}
          defaultValue="Alcohol by Volume"
        />

        <Select
          name="bitterness"
          value={filterBy.ibu || ''}
          disabled={!possibleFilters.ibu}
          ariaLabel="Filter by Bitterness"
          onChange={(e) => setFiltersBy('ibu', Number(e.target.value))}
          options={possibleFilters.ibu || []}
          defaultValue="Bitterness"
        />
      </div>
    </div>
  )
}
