import useListBeersStore, { SortByType } from '@/app/store/useListBeers'

const sortingOptions = [
  { value: '', label: 'Sorting by' },
  { value: 'nameAZ', label: 'Name - A from Z' },
  { value: 'nameZA', label: 'Name - Z from A' },
  { value: 'brewingDateAsc', label: 'Brewing Date - Ascending' },
  { value: 'brewingDateDesc', label: 'Brewing Date - Descending' },
];

function Sorting() {
  const sortBy = useListBeersStore((state) => state.sortBy)
  const setSortBy = useListBeersStore((state) => state.setSortBy)

  return (
    <div className='flex justify-end w-full'>
      <select
        name="sortBy"
        value={sortBy}
        aria-label="Sort Beers By"
        onChange={(e) => setSortBy(e.target.value as SortByType)}
        className="flex w-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-[#ef9c0a] focus:border-[#ef9c0a] lg:w-[256px] p-2 outline outline-transparent"
      >
        {sortingOptions.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  )
}

export default Sorting