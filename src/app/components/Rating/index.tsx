'use client'

import { useEffect, useState } from 'react'
import { Rating as RatingStar } from 'react-simple-star-rating'

import { SelectedBeer } from '@/models/Beer'
import useSelectedBeersStore from '@/app/store/useSelectedBeers'

type RatingProps = {
  beerId: number,
  readonly: boolean
}

const BeerRatingText = ({ isClient, currentBeer }: {
  isClient: boolean,
  currentBeer?: SelectedBeer
}) => {
  if (!isClient) {
    return null;
  }

  if (currentBeer?.rate) {
    return (
      <span className='text-4xl text-[#1e1e1e] font-light'>
        {Number(currentBeer.rate).toFixed(1)}
      </span>
    );
  }

  return (
    <span className='text-4xl text-gray-400 font-light'>-·-</span>
  );
};

export default function Rating({ beerId, readonly }: RatingProps) {
  const [isClient, setIsClient] = useState(false)

  const addRate = useSelectedBeersStore(state => state.addRate)
  const selectedBeers = useSelectedBeersStore(state => state.selectedBeers)
  const currentBeer = selectedBeers.find((beer) => beer.id === beerId)

  useEffect(() => {
    setIsClient(true)
  }, [])

  const handleRating = (value: number) => {
    addRate(beerId, value)
  }

  return (
    <div className='flex flex-col items-center'>
      <BeerRatingText isClient={isClient} currentBeer={currentBeer} />
      <RatingStar
        size={26}
        allowFraction
        initialValue={isClient ? currentBeer?.rate || 0 : 0}
        onClick={handleRating}
        SVGstyle={{ display: "inline-block" }}
        readonly={readonly}
        className='cursor-pointer'
      />
    </div>
  )
}
