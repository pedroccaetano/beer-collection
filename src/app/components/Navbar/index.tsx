import Link from 'next/link';
import Image from 'next/image';

import ButtonSearch from '@/app/components/Navbar/ButtonSearch';

function Navbar() {
  return (
    <header className="bg-white sticky top-0 z-10 border-b shadow-sm mb-4 md:mb-6 px-3 xl:px-0">
      <nav
        className="flex flex-col gap-4 sm:flex-row sm:justify-between items-center py-2 font-bold max-w-6xl mx-auto"
        role="navigation"
        aria-label="Main Navigation"
      >
        <Link href="/" className="flex justify-center items-center gap-4 whitespace-nowrap">
          <div className="w-14">
            <Image
              src="/logo.png"
              alt="Beer Collection - Logo"
              width="0"
              height="0"
              sizes="100vw"
              className="w-full h-auto"
            />
          </div>
          <span className="text-2xl text-[#2e2015] block">Beer Collection</span>
        </Link>

        <ButtonSearch />
      </nav>
    </header>
  )
}

export default Navbar