'use client'

import { usePathname } from 'next/navigation';

import { PlusCircle } from '@/lib/icons'
import useSearchStore from '@/app/store/useSearchDialog'

function ButtonSearch() {
  const pathname = usePathname();
  const { open } = useSearchStore()

  if (pathname !== '/') return null

  return (
    <button
      onClick={open}
      className='bg-[#ef9c0a] text-white rounded-lg font-medium border border-input h-9 text-sm px-6 flex items-center justify-center border-none gap-2 hover:bg-[#ef9b0ae7] transition-colors shadow-sm'
    >
      <PlusCircle size={24} weight="light" />
      Add New Beer
    </button>
  )
}

export default ButtonSearch