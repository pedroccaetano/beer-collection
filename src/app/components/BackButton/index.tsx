'use client'

import { KeyboardEvent } from 'react'
import { useRouter } from 'next/navigation'

import { ArrowLeft } from '@/lib/icons'

function BackButton() {
  const { push } = useRouter()

  const handleKeyDown = (e: KeyboardEvent<HTMLSpanElement>) => {
    if (e.key === 'Enter' || e.key === 'Space') {
      push('/');
    }
  }

  const handleClick = () => {
    push('/')
  }

  return (
    <span
      onClick={handleClick}
      className='cursor-pointer'
      onKeyDown={handleKeyDown}
      role="button"
      tabIndex={0}
      aria-label="Back to the previous page"
    >
      <ArrowLeft size={32} weight="light" color='gray' />
    </span>
  )
}

export default BackButton
