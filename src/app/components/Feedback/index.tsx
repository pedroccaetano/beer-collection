'use client';

import { useEffect, useState } from 'react';

import useSelectedBeersStore from '@/app/store/useSelectedBeers';

type Props = {
  beerId: number;
};

export default function Feedback({ beerId }: Props) {
  const [comment, setComment] = useState('');
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    const commentSelected = useSelectedBeersStore.getState().selectedBeers.find((beer) => beer.id === beerId)?.comment || '';
    setDisabled(commentSelected.length > 1)
    setComment(commentSelected)
  }, [beerId])

  const handleComment = () => {
    if (disabled) {
      setDisabled(false);
      return
    }

    useSelectedBeersStore.getState().addComment(beerId, comment);
    setDisabled(true);
  }

  return (
    <div>
      <textarea
        rows={5}
        value={comment}
        disabled={disabled}
        name={`comment-${beerId}`}
        aria-label={`Comment for beer ${beerId}`}
        onChange={(e) => setComment(e.target.value)}
        placeholder="Add here what you think about this beer!"
        className="bg-gray-50 w-full py-2 px-3 border border-gray-300 rounded placeholder-gray-400 resize-none"
      ></textarea>
      {comment !== '' && (
        <div className='flex justify-end'>
          <button
            onClick={handleComment}
            className='bg-[#ef9c0a] text-white rounded-lg font-medium border border-input h-9 text-sm px-6 flex items-center justify-center border-none gap-2 hover:bg-[#ef9b0ae7] transition-colors shadow-sm'
          >
            {disabled ? 'Edit Comment' : 'Save Comment'}
          </button>
        </div>
      )}
    </div>
  );
}
