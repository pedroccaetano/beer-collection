'use client'

import useSearchStore from '@/app/store/useSearchDialog'
import SearchDialog from './SearchDialog'

function Search() {
  const { state } = useSearchStore()

  return state && <SearchDialog />
}

export default Search