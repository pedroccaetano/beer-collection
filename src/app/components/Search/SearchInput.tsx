'use client'

import { ChangeEvent, use, useEffect, useState } from 'react';
import { useDebounce } from '@uidotdev/usehooks';

import { X } from '@/lib/icons'
import { fetchBeers } from '@/lib/fetchBeers';
import { BeersResults } from '@/models/Beer';
import useSearchStore from '@/app/store/useSearchDialog'
import SearchItem from '@/app/components/Search/SearchItem';

function SearchInput() {
  const [search, setSearch] = useState('');
  const [isSearching, setIsSearching] = useState(true);

  const { close } = useSearchStore()
  const debouncedSearchTerm = useDebounce(search, 1000);

  const [searchResults, setSearchResults] = useState<BeersResults>([]);
  const [initialResults, setInitialResults] = useState<BeersResults>([]);

  useEffect(() => {
    const searchAllBeers = async () => {
      const beersInitialResults = await fetchBeers("https://api.punkapi.com/v2/beers");
      if (beersInitialResults) {
        setInitialResults(beersInitialResults);
        setSearchResults(beersInitialResults);
      }
      setIsSearching(false);
    };

    searchAllBeers();
  }, []);

  useEffect(() => {
    const searchBeers = async () => {
      setIsSearching(true);
      if (debouncedSearchTerm) {
        const data = await fetchBeers(`https://api.punkapi.com/v2/beers?beer_name=${debouncedSearchTerm}`);
        setSearchResults(data || []);
      } else {
        setSearchResults(initialResults);
      }
      setIsSearching(false);
    };

    searchBeers()
  }, [debouncedSearchTerm, initialResults]);

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    setSearch(e.target.value)
  }

  const handleClose = () => {
    setSearch('')
    close()
  }

  return (
    <>
      <div>
        <input
          autoFocus
          type="text"
          value={search}
          onChange={handleSearch}
          placeholder="Search Beer"
          className="border border-[#ef9c0a] w-full rounded-t rounded-b-none rounded-md h-14 p-4 pr-10 focus:outline-[#ef9c0a]"
        />
        <button
          onClick={handleClose}
          className="absolute right-4 top-3 text-[#ef9c0a] hover:text-[#ff5733] cursor-pointer p-1"
        >
          <X size={22} />
        </button>
      </div>
      <div className='max-h-[300px] bg-white overflow-y-auto overflow-x-hidden border-[#eae0da] border rounded-b-lg'>
        <div className='flex flex-col gap-2 p-2'>
          {isSearching ? (
            <span>Searching.</span>
          ) : searchResults.length > 0 ? (
            searchResults.map((beer) => <SearchItem beer={beer} key={beer.id} />)
          ) : (
            <span>No results found.</span>
          )}
        </div>
      </div>
    </>
  )
}



export default SearchInput