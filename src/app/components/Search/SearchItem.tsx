import Image from 'next/image';

import { Beer } from '@/models/Beer';
import useSelectedBeersStore from '@/app/store/useSelectedBeers';
import useSearchDialogStore from '@/app/store/useSearchDialog';

type SearchItemProps = {
  beer: Beer
}

function SearchItem({ beer }: SearchItemProps) {
  const { add } = useSelectedBeersStore()
  const { close } = useSearchDialogStore()

  const handleAddBeer = () => {
    add(beer)
    close()
  }

  return (
    <div
      className='min-h-14 h-full flex cursor-pointer select-none hover:bg-slate-100 rounded-lg items-center px-4 py-1.5 text-sm outline-none'
      onClick={handleAddBeer}
    >
      <div className="h-full max-w-[16px] flex justify-center">
        <Image
          src={beer.image_url}
          alt={beer.name}
          width="0"
          height="0"
          sizes="100vw"
          className="w-full h-auto"
        />
      </div>
      <div className='ml-4'>
        <h2 className="text-base font-normal text-[#1e1e1e]">{beer.name}</h2>
        <h2 className="text-sm font-normal text-gray-600">{beer.tagline}</h2>
      </div>
    </div>
  )
}

export default SearchItem