"use client";

import { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';

import SearchInput from './SearchInput';

function SearchDialog() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  return mounted ?
    <>
      {createPortal(
        <div className="fixed inset-0 z-50 backdrop-blur-sm" >
          <div className="fixed left-[50%] top-[50%] grid w-full max-w-lg translate-x-[-50%] translate-y-[-50%]">
            <SearchInput />
          </div>
        </div>
        ,
        document.body
      )}
    </> : null;
}

export default SearchDialog