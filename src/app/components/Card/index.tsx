
import Image from 'next/image';
import { KeyboardEvent } from 'react';
import { useRouter } from 'next/navigation';

import { Beer } from '@/models/Beer';
import { BeerStein } from '@/lib/icons'
import Rating from '@/app/components/Rating';

type CardProps = {
  beer: Beer,
}

function Card({ beer }: CardProps) {
  const router = useRouter()

  const handleClick = () => {
    router.push(`/beer/${beer.id}`)
  }

  const handleKeyDown = (e: KeyboardEvent<HTMLSpanElement>) => {
    if (e.key === 'Enter' || e.key === 'Space') {
      handleClick();
    }
  }

  return (
    <article
      tabIndex={0}
      role="button"
      onClick={handleClick}
      onKeyDown={handleKeyDown}
      className="w-full min-h-[250px] bg-white rounded-xl border border-[#eae0da] hover:shadow-xl cursor-pointer transition-shadow duration-300 ease-in-out"
    >
      <header className="flex content-between h-full">
        <div className="h-full min-w-[128px] flex items-center content-center justify-center">
          <Image src={beer.image_url} priority alt={beer.name} height={200} width={70} className="p-1 filter drop-shadow-2xl" />
        </div>
        <div className="p-4 flex flex-col w-full justify-between">
          <section className="flex justify-between">
            <div>
              <h2 className="text-lg font-normal text-[#1e1e1e]">{beer.name}</h2>
              <h3 className="text-lg font-medium text-[#1e1e1e] overflow-hidden line-clamp-2">{beer.tagline}</h3>
            </div>
            <Rating beerId={beer.id} readonly={true} />
          </section>
          <section className="bg-gray-50 p-4 rounded-xl text-[#1e1e1e] flex items-start">
            <span className="inline-block mr-4" aria-hidden="true">
              <BeerStein
                size={22}
                weight="fill"
                color="#ef9c0a"
              />
            </span>
            <div>
              <p className="overflow-hidden line-clamp-3">{beer.description}</p>
            </div>
          </section>
        </div>
      </header>
    </article>
  );
}

export default Card
