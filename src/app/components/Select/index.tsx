import React, { ChangeEvent } from 'react';

interface SelectProps {
  name: string;
  value: string | number;
  disabled: boolean;
  ariaLabel: string;
  onChange: (e: ChangeEvent<HTMLSelectElement>) => void;
  defaultValue?: string;
  options: (string | number)[];
}

const selectClass = "flex w-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-[#ef9c0a] focus:border-[#ef9c0a] lg:w-[256px] p-2 outline outline-transparent"

function Select(props: SelectProps) {
  const { name, value, disabled, ariaLabel, onChange, options, defaultValue } = props;

  return (
    <select
      name={name}
      value={value}
      disabled={disabled}
      aria-label={ariaLabel}
      onChange={onChange}
      className={selectClass}
    >
      {defaultValue && <option value="">{defaultValue}</option>}
      {options.map((option) => (
        <option key={`${option}_${name}`} value={option}>
          {option}
        </option>
      ))}
    </select>
  );
}

export default Select;
