import { Metadata, ResolvingMetadata } from 'next'
import Image from 'next/image'

import { Beer } from '@/models/Beer'
import { BeerStein } from "@/lib/icons"
import Rating from '@/app/components/Rating'
import { fetchBeers } from "@/lib/fetchBeers"
import Feedback from '@/app/components/Feedback'
import BackButton from '@/app/components/BackButton'

type Props = {
  params: {
    id: string
  }
}

async function getBeerData(id: string): Promise<Beer | null> {
  const beers = await fetchBeers(`https://api.punkapi.com/v2/beers/${id}`);

  if (beers?.length === 1) {
    const beer = beers[0];
    return beer;
  }

  return null;
}

export async function generateMetadata({ params }: Props): Promise<Metadata | ResolvingMetadata> {
  const beer = await getBeerData(params.id);

  const title = beer
    ? `${beer.name} | Beer Details`
    : 'Beer Details';

  return {
    title,
  };
}

async function Page({ params }: Props) {
  const beer = await getBeerData(params.id);

  if (!beer) {
    return null
  }

  return (
    <div>
      <BackButton />
      <div className='flex flex-row rounded-xl border border-[#eae0da] p-4 md:p-8  mt-4 md:mt-6'>
        <div className='min-w-[200px] justify-center hidden md:flex'>
          <Image src={beer.image_url} alt={beer.name} height={120} width={120} priority className='filter drop-shadow-2xl max-h-[457px]' style={{ width: 'auto', height: 'auto' }} />
        </div>
        <div className='md:ml-8 flex flex-col gap-8 md:gap-14 flex-1'>
          <div className='flex items-start gap-4 flex-row'>
            <div className='min-w-[150px] flex items-center justify-center md:hidden'>
              <Image src={beer.image_url} alt={beer.name} height={60} width={60} priority className='filter drop-shadow-2xl' style={{ width: 'auto', height: 'auto' }} />
            </div>
            <div className='flex flex-1 flex-col gap-6 items-start md:flex-row md:justify-between'>
              <div>
                <h1 className='text-2xl font-bold'>{beer.name}</h1>
                <p className='text-lg mt-2'>{beer.tagline}</p>
              </div>
              <Rating beerId={beer.id} readonly={false} />
            </div>
          </div>
          <div className="bg-gray-50 p-4 rounded-xl text-[#1e1e1e] flex items-start">
            <span className="inline-block mr-4" aria-hidden="true">
              <BeerStein
                size={22}
                weight="fill"
                color="#ef9c0a"
              />
            </span>
            <div>
              <p>{beer.description}</p>
            </div>
          </div>
          <Feedback beerId={beer.id} />
        </div>
      </div>
    </div>
  )
}



export default Page
