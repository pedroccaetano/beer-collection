'use client'

import { useEffect, useState } from 'react';

import Card from '@/app/components/Card';
import useListBeersStore from '@/app/store/useListBeers';
import useSearchDialogStore from '@/app/store/useSearchDialog';
import useSelectedBeersStore from '@/app/store/useSelectedBeers';
import FilterAndSorting from '@/app/components/FilterAndSort';

function Home() {
  const [loading, setLoading] = useState(true);
  const beers = useListBeersStore((state) => state.beers)
  const openSearch = useSearchDialogStore((state) => state.open)
  const initialBeers = useListBeersStore((state) => state.initialBeers)

  useEffect(() => {
    const fetchBeers = useSelectedBeersStore.getState().fetchBeers
    fetchBeers()
    setLoading(false)
  }, [])

  return (
    <div>
      <FilterAndSorting />
      {loading ? (
        <div className="text-center mt-10">
          <h2 className="text-2xl font-bold mb-4">Loading...</h2>
        </div>
      ) : initialBeers.length === 0 ? (
        <div className="text-center mt-10 border border-[#eae0da] shadow-md rounded-lg py-9">
          <h2 className="text-2xl font-bold mb-4">{"Oops! It looks like you haven't selected any beers."}</h2>
          <p className="text-gray-600 text-lg">Click the button below to find and select your favorite beers.</p>
          <button
            className="mt-6 px-4 py-2 text-[#ef9c0a] border border-[#ef9c0a] rounded-lg hover:bg-[#ef9c0a] hover:text-white transition-colors duration-300 ease-in-out"
            onClick={openSearch}
          >
            Find your Beers
          </button>
        </div>
      ) : beers.length === 0 ? (
        <div className="text-center mt-10">
          <p className="text-gray-600 text-lg">No matching beers found. Please refine your filters.</p>
        </div>
      ) : (
        <section className="gap-6 grid grid-cols-1 md:grid-cols-2">
          {beers.map((beer) => <Card beer={beer} key={beer.id} />)}
        </section>
      )}
    </div>
  )
}

export default Home