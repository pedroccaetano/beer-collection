import './styles/globals.css'

import type { Metadata } from 'next'
import { Inter } from 'next/font/google'

import Navbar from '@/app/components/Navbar'
import Search from "@/app/components/Search";

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Beer Collection',
  description: 'Track your beer collection',
  icons: {
    icon: ['/favicon.ico'],
  },
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Navbar />
        <main className='max-w-6xl mx-auto px-3 xl:px-0'>
          {children}
        </main>
        <Search />
      </body>
    </html>
  )
}
