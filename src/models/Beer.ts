import { z } from "zod";

export const BeerSchema = z.object({
  id: z.number(),
  name: z.string(),
  tagline: z.string(),
  first_brewed: z.string(),
  formatted_first_brewed: z.date().optional(),
  description: z.string(),
  image_url: z.string(),
  abv: z.number(),
  ibu: z.number(),
  target_fg: z.number(),
  target_og: z.number(),
  ebc: z.number(),
  srm: z.number(),
  ph: z.number(),
  attenuation_level: z.number(),
  volume: z.object({
    value: z.number(),
    unit: z.string(),
  }),
  boil_volume: z.object({
    value: z.number(),
    unit: z.string(),
  }),
  method: z.object({
    mash_temp: z.array(
      z.object({
        temp: z.object({
          value: z.number(),
          unit: z.string(),
        }),
        duration: z.number(),
      })
    ),
    fermentation: z.object({
      temp: z.object({
        value: z.number(),
        unit: z.string(),
      }),
    }),
  }),
  ingredients: z.object({
    malt: z.array(
      z.object({
        name: z.string(),
        amount: z.object({
          value: z.number(),
          unit: z.string(),
        }),
      })
    ),
    hops: z.array(
      z.object({
        name: z.string(),
        amount: z.object({
          value: z.number(),
          unit: z.string(),
        }),
        add: z.string(),
        attribute: z.string(),
      })
    ),
    yeast: z.string(),
  }),
  food_pairing: z.array(z.string()),
  brewers_tips: z.string(),
  contributed_by: z.string(),
});

export type Beer = z.infer<typeof BeerSchema>;

export type BeersResults = Beer[];

const SelectedBeerSchema = z.object({
  id: z.number(),
  rate: z.number().optional(),
  comment: z.string().optional(),
});

export type SelectedBeer = z.infer<typeof SelectedBeerSchema>;
