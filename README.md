# Project - Beer Collection

This project is a part of the Frontend Case: Beer Collection, created to fulfill specific needs.

## 💻 Demo

Experience the project live by visiting the following link:

[**App Demo**](https://beer-collection-two.vercel.app/)

We hope you enjoy it! =)

## 🚀 Getting Started

1. Clone the Repository

2. Install Dependencies

Use `pnpm` to install all the project dependencies.

```bash
pnpm install
```

3. run the development server:

```bash
pnpm dev
```

You can access the project a [http://localhost:3000](http://localhost:3000) in your web browser.

## 🧰 Stack

This project utilizes the following technologies:

- [Next.js 13](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Tailwindcss](https://tailwindcss.com/)
- [Zod](https://zod.dev/): schema validation
- [usehooks](https://usehooks.com/): includes the `useDebounce` hook for efficient debouncing in React.
- [zustand](https://github.com/pmndrs/zustand#typescript-usage): state management library used.
- [react-simple-star-rating](https://github.com/awran5/react-simple-star-rating): star rating component

## 📝 Additional Information

### Technical Details

- This project includes an `.nvmrc` file. if you need to know the specific version it was created, you can check this file.

- Package Manager: I managed the project with `pnpm`. You can check more about it [here](https://pnpm.io/).

### Handle Data

- State Management: I use 'Zustand' to handle our application's data efficiently. It keeps things simple and organized.

### Making Sure Data Is Right

- Schema Validation: I use 'Zod' to check if the data follows a certain structure.

### Rating System

- React Simple Star Rating: I let users rate beers with this feature. It's simple and user-friendly.
